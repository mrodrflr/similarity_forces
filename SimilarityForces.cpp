#include "SystemFunctions.h"
#include <ncurses.h>

int main(int argc, char **argv)
{
	//Initialize System Structures
	BodiesSystem system = BodiesSystem(parseArgs(argc, argv));

	//Initialize System and Begin Simulation
	if (!initializeSystem(&system)) goto SimExit;
	if (!simulateSystem(&system)) goto SimExit;

	//Free Resources
	destroySystem(&system);

	SimExit:
	//Exit Program
	return 0;
}

//Parse Command Line Arguments
Properties parseArgs(int argc, char **argv)
{
	Properties props = Properties();
	RGG rgg = RGG();
	props.rgg = rgg;
	int c, longIndex;

	//Single-character options
	static const char *optString = "L:N:T:e:m:F:i:w:p:h";
	//Multi-character options
	static const struct option longOpts[] = {
		{ "box",	required_argument,	NULL, 'L' },
		{ "nodes",	required_argument,	NULL, 'N'  },
		{ "slots",	required_argument,	NULL, 'T' },
		{ "mu_2", required_argument, 		NULL, 'e' },
		{ "help",	no_argument,		NULL, 'h' },
		{ "f_zero",	required_argument,	NULL, 'F' },
		{ "mu_1",	required_argument,	NULL, 'm' },
		{ "input", required_argument, NULL, 'i' },
		{ "warmup", required_argument,  	NULL, 'w' },
		{ "probability", required_argument,  	NULL, 'p' },
		{ "dump", 	no_argument, 		NULL,	0 },
		{ NULL,		0,			0,     0  }
	};

	try {
		while ((c = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1) {
			switch (c) {
			case 'L':	//L, Size of space box LxL
				props.L = atoi(optarg);
				if (props.L <= 0)
					throw DynamicsException("Invalid argument for 'L' parameter!\n");
				break;
			case 'N':	//N, Number of Agents/Nodes
				props.N = atoi(optarg);
				if (props.N <= 0)
					throw DynamicsException("Invalid argument for 'N' parameter!\n");
				break;
			case 'T':	//Number of time slots
				props.slots = atoi(optarg);
				if (props.slots <= 0)
					throw DynamicsException("Invalid argument for 'T' parameter!\n");
				break;
			case 'e': //mu_2 exponent for Escaping Probability
				props.mu2 = atof(optarg);
				if (props.mu2 <= 0)
					throw DynamicsException("Invalid argument for 'mu_2' parameter!\n");
				break;
			case 'F': //force magnitude F0
				props.F0 = atof(optarg);
				if (props.F0 < 0)
					throw DynamicsException("Invalid argument for 'F0' parameter!\n");
				break;
			case 'm': //mu_1 - Forces exponent
				props.mu1 = atof(optarg);
				if (props.mu1 <= 0)
					throw DynamicsException("Invalid argument for 'mu_1' parameter!\n");
				break;
			case 'i': //input file with initial node coordinates
				props.file_provided = true;
				props.input_file = optarg;
				break;
			case 'w': //warmup - Number of slots for warmup period
				props.warmup_time = atoi(optarg);
				if (props.warmup_time > 0)
					props.warmup = true;
				else
					props.warmup = false;
				if (props.warmup_time < 0)
					throw DynamicsException("Invalid argument for 'warmup' parameter!\n");
				break;
			case 'p': //probability - Assigns the same probaility to all nodes for activation
				props.p = atof(optarg);
				props.p_flag = true;
				if (props.p < 0 || props.p > 1)
					throw DynamicsException("Invalid argument for 'probability' parameter!\n");
				break;
			case 0:
				if(!strcmp("dump",longOpts[longIndex].name)){//Dump initial position coordinates - If warmup period is on the coordinates dumped will be after the warmup period
					props.dump = true;
				}
				break;
			case 'h':	//Help menu
				printf("\nUsage  :  program [options]\n\n");
				printf("Program Options...........................\n");
				printf("===========================================\n");
				printf("Flag:\t\t\tMeaning:\n");
				printf("  -N, \t\t\tNumber of Nodes (> 0)\n");				
				printf("  -L, \t\t\tSize of Euclidean space (LxL) (> 0)\n");				
				printf("  -T, --slots\t\tNumber of time slots to simulate (> 0)\n");
				printf("  -e, --mu_1\t\tExponent for exponential decay of bonding forces (> 0)\n");
				printf("  -m, --mu_2\t\tExponent for exponential decay of attractive forces (> 0)\n");
				printf("  -F, --f_zero\t\tForce magnitude (>= 0)\n");
				printf("  -w, --warmup\t\tWarmup period duration in time slots (>= 0)\n");
				printf("  -p, --probability\tAssigns the same activation probaility to all nodes (0 < p <= 1) If omitted, an activation probability is sampled uniformly at random for each node.\n");
				printf("  -i, --input\t\tInput file to preset the initial coordinates in the Euclidean space and in the similarity space.\n");
				printf("  --dump\t\tCreates an output file named 'initial_positions.txt': If there is no warmup period, it contains the initial positions of nodes in the Euclidean space. If there is a warmup period, then it contains the positions of nodes in the Euclidean space in the slot immediately after the warmup period. In both cases the file also contains the corresponding similarity coordinates of nodes.\n");
				printf("\n");
				printf("Information...............................\n");
				printf("===========================================\n");
				printf("This program is the FDM simulator from the paper 'Similarity forces and recurrent components in human face-to-face interaction networks', http://arxiv.org/abs/1808.00750\n");
				printf("Authors of paper and this code: Marco Antonio Rodriguez Flores, Fragkisko Papadopoulos\n"); 
				printf("Contact: mj.rodriguezflores@edu.cut.ac.cy\n");
				printf("August, 2018\n");
				exit(0);
			case ':':
				//Single-character flag needs an argument
				if (!!optopt)
					fprintf(stderr, "%s : option '-%c' requires an argument.\n", argv[0], optopt);
				else
					fprintf(stderr, "%s : option '%s' requires an argument.\n", argv[0], argv[optind-1]);
				exit(3);
			case '?':	//Unrecognized flag
			default:	//Default case
				if (!!optopt)
					fprintf(stderr, "%s : option '-%c' is not recognized.\n", argv[0], optopt);
				else
					fprintf(stderr, "%s : option '%s' is not recognized.\n", argv[0], argv[optind-1]);
				exit(4);
			}
		}
	} catch (DynamicsException d) {
		fprintf(stderr, "DynamicsException in %s: %s on line %d\n", __FILE__, d.what(), __LINE__);
		exit(1);
	} catch (std::exception e) {
		fprintf(stderr, "Unknown exception in %s: %s on line %d\n", __FILE__, e.what(), __LINE__);
		exit(2);
	}
	
	props.d = 1.0; //Interaction radius
	props.v = 1.0; //Random displacement
	
	return props;
}

bool initializeSystem(BodiesSystem * const system)
{
	if (!allocateMemory(system->bodies, system->props.N))
		return false;

	system->props.rgg.resize(system->props.N); //Initialize Adjacency Matrix	
	initializeAgents(system->bodies, system->props.N, system->props.L, system->props.d, system->props.mu2, system->props.rgg, system->props.mu1, system->props.file_provided, system->props.dump, system->props.warmup, system->props.p_flag, system->props.input_file, system->props.p);

	return true;
}

bool simulateSystem(BodiesSystem * const system)
{
	openEdgesFile();

	double displacement = 0;
	double total_interacting = 0;
	std::pair<double, double> slot_pair = std::make_pair(0.0, 0.0);
	int t = 1;
	while(system->props.warmup || (t <= system->props.slots)){

		slot_pair = simulateSlot(system->bodies, system->props.N, system->props.L, system->props.d, system->props.rgg, system->props.F0, system->props.dump, system->props.warmup, system->props.mu2, system->props.mu1, system->props.v, system->props.warmup_time, t);
		
		total_interacting += slot_pair.first;
		displacement += slot_pair.second;
		
		if(!system->props.warmup){
			makeSnapshot(system->bodies, system->props.N, t, system->props.rgg);
			t++;
		}
		else{
			displacement = 0;
			total_interacting = 0;
		}
	}
	
	closeEdgesFile();
	mvprintw(6, 0, "*******************************************************************************");
	mvprintw(7, 0, "\tAverage displacement per slot: %f\n", displacement/system->props.slots);
	mvprintw(8, 0, "\tAverage interacting nodes per slot: %f\n", total_interacting/system->props.slots);
	mvprintw(9, 0, "Simulation Completed. Press any key to close the program.");
	refresh();
	getchar();

	return true;
}

void destroySystem(BodiesSystem * const system)
{
	releaseMemory(system->bodies, system->props.N);
}

