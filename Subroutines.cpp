#include "Subroutines.h"
#include <map>

std::ofstream list_file;
std::stringstream sstml;

bool safe_malloc(void **data, const size_t nb)
{
	try {
		*data = malloc(nb);
		if (*data == NULL)
			throw std::bad_alloc();
		memset(*data, 0, nb);
	} catch (std::bad_alloc()) {
		fprintf(stderr, "Memory allocation failure in %s on line %d!\n", __FILE__, __LINE__);
		*data = NULL;
		return false;
	}

	return true;
}

void safe_free(void **data, const size_t nb)
{
	free(*data);
	*data = NULL;
}

//Opens temporal network output file
void openEdgesFile(){
	sstml << "temporal_edges.txt";
	list_file.open(sstml.str().c_str());
	list_file.precision(10);
}
//Closes temporal network output file
void closeEdgesFile(){
	list_file.flush();
	list_file.close();
}

//Prints Initial Agent Positions
void dumpPositions(const Bodies &b, const int &N){
	std::ofstream output_file;
	std::stringstream ssp;
	
	ssp << "initial_positions.txt";
	output_file.open(ssp.str().c_str());
	output_file.precision(10);
	
	if(output_file.is_open()){
		for(int i=0;i<N;i++){
			output_file << b.position->x(i) << " " << b.position->y(i) << " " << b.theta[i] << "\n";
		}
		
		output_file.flush();
		output_file.close();
	}
	else
		fprintf(stderr, "Failed to open file: %s\n", ssp.str().c_str());
}

//Prints individual files per slot, one for the agents positions and one for the edges between them.
void makeSnapshot(const Bodies &b, const int &N, const int &time, const RGG rgg)
{
	std::ofstream output_file, edge_list;
	std::stringstream sstm, sstm2;
	int i;

	sstm << "snapshots/snapshot_positions_" << time << ".dat";
	output_file.open(sstm.str().c_str());
    	output_file.precision(10);
    
	if (output_file.is_open()) {
		for (i = 0; i < N; i++) {
				output_file << b.position->x(i) << " " << b.position->y(i) << "\n";
		}

		output_file.flush();
		output_file.close();
	} else
		fprintf(stderr, "Failed to open file: %s\n", sstm.str().c_str());
	
	sstm2 << "snapshots/snapshot_edges_" << time << ".edg";
	edge_list.open(sstm2.str().c_str());
	edge_list.precision(10);
	int j;
		
	if(edge_list.is_open()){
		for(i = 0; i < N; i++){
			for(j = i+1; j < N; j++){
				if(rgg.adj[i][j] == 1){
					edge_list << i << " " << j << "\n";
					if(list_file.is_open())
						list_file << time << " " << i << " " << j << "\n";
				}
			}
		}
		edge_list.flush();
		edge_list.close();
	}else
		fprintf(stderr, "Failed to open file: %s\n", sstm2.str().c_str());
}
//Tests wether agent i and j are within interaction distance or not.
bool testProximity(double x_i, double y_i, double x_j, double y_j, double d) {
	double distance = sqrt(pow((x_j - x_i), 2) + pow((y_j - y_i), 2));
	if(distance <= d)
		return true;
	return false;
}

double s1_distance(double theta_i, double theta_j, const int N){
	double R = N/(2*M_PI);
	double dtheta = fabs(theta_i - theta_j);
	if (dtheta > M_PI)
		dtheta = 2*M_PI - dtheta;

	return R*dtheta;	
}

double bonding_force(double theta_i, double theta_j, double mu2, const int N){
	double R_dtheta = s1_distance(theta_i, theta_j, N);
	return exp(-R_dtheta/mu2);
}

void initializeEdges(Bodies &bodies, const int N, const double d, RGG &rgg, const double mu2){
	std::map<int, float> node_degree;
	for(int i = 0; i < N; i++){
		if(!node_degree.count(i)) node_degree[i] = 0;
		for(int j = i+1; j < N; j++){
			if(!node_degree.count(j)) node_degree[j] = 0;
			if(testProximity(bodies.position->x(i), bodies.position->y(i), bodies.position->x(j), bodies.position->y(j), d) && bodies.active[i] && bodies.active[j])
			{
				
				bodies.not_isolated[i] = true;
				bodies.not_isolated[j] = true;
				
				rgg.adj[i][j] = 1;
				rgg.adj[j][i] = 1;
				
				//AVERAGE bonding forces
				double similarity_ij = bonding_force(bodies.theta[i], bodies.theta[j], mu2, N);
				bodies.rela[i] += similarity_ij;
				bodies.rela[j] += similarity_ij;
				
				node_degree[i]++;
				node_degree[j]++;
			}
			else{				
				rgg.adj[i][j] = 0;
				rgg.adj[j][i] = 0;
			}
		}	
		
		if(node_degree[i] != 0)
			bodies.rela[i] = bodies.rela[i] / node_degree[i]; //Average bonding force to neighbors
		
	}
}

void findEdges(Bodies &bodies, const int N, const double d, RGG &rgg, int &active_links, int &interacting_nodes, const double mu2){
	std::map<int, float> node_degree;
	for(int i = 0; i < N; i++){
		if(!node_degree.count(i)) node_degree[i] = 0;
		for(int j = i+1; j < N; j++){
			if(!node_degree.count(j)) node_degree[j] = 0;
			if(testProximity(bodies.position->x(i), bodies.position->y(i), bodies.position->x(j), bodies.position->y(j), d) && bodies.active[i] && bodies.active[j])
			{
				active_links++;
				if(!bodies.slot_interaction[i]){
					interacting_nodes++;
				}
				if(!bodies.slot_interaction[j]){
					interacting_nodes++;
				}	

				bodies.slot_interaction[i] = true;
				bodies.slot_interaction[j] = true;

				rgg.adj[i][j] = 1;
				rgg.adj[j][i] = 1;	

				bodies.not_isolated[i] = true;
				bodies.not_isolated[j] = true;
				
				//AVERAGE bonding forces
				double similarity_ij = bonding_force(bodies.theta[i], bodies.theta[j], mu2, N);
				bodies.rela[i] += similarity_ij;
				bodies.rela[j] += similarity_ij;

				node_degree[i]++;
				node_degree[j]++;
			}
			else{
					rgg.adj[i][j] = 0;
					rgg.adj[j][i] = 0;
			}
		}
		
		if(node_degree[i] != 0)
			bodies.rela[i] = bodies.rela[i] / node_degree[i]; //Average bonding force to neighbors
		
	}
}

void updateForces(Bodies &bodies, RGG &rgg, const int N, const double F0, const double mu1){
	memset(bodies.F->x(), 0, sizeof(double) * N);
	memset(bodies.F->y(), 0, sizeof(double) * N);
	
	for (int i = 0; i < N; i++){
		if(!bodies.active[i])
			continue;
		for (int j = i + 1; j < N; j++){
			if(!bodies.active[j])
				continue;
			
			std::pair<double, double> Force = std::make_pair(0.0, 0.0);

			double R_dtheta = s1_distance(bodies.theta[i], bodies.theta[j], N);	
			Force = pairwiseForce(bodies.position->x(i), bodies.position->x(j), bodies.position->y(i), bodies.position->y(j), R_dtheta, F0, mu1);
			
			bodies.F->x(i) += Force.first;
			bodies.F->y(i) += Force.second;
			bodies.F->x(j) -= Force.first;
			bodies.F->y(j) -= Force.second; 
		}
	}
}

//pairwise attractive force formula
std::pair<double, double> pairwiseForce(const double x_i, const double x_j, const double y_i, const double y_j, double R_dtheta, const double F0, const double mu1)
{
	double rji[2]; //rji[] is the vector from particle i to particle j
	
	rji[0] = x_j - x_i; //x coords
	rji[1] = y_j - y_i; //y coords
	
	double r2 = rji[0] * rji[0] + rji[1] * rji[1];
	double r = sqrt(r2); 	//|rji|

	double Xi = (R_dtheta);
    	double denominator = exp(Xi/mu1) * r;
	
	double F_x = F0*rji[0] / denominator;
	double F_y = F0*rji[1] / denominator;
	
	return std::make_pair(F_x, F_y); 
}
