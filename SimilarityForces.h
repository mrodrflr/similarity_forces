//Core System Files
#include <cstring>
#include <exception>
#include <fstream>
#include <getopt.h>
#define __STDC_FORMAT_MACROS
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdio.h>
#include <vector>

#define HALF_PI	1.57079632679489661923
#define TWO_PI	6.28318530717958647692

//Vectorized Data Types
struct __attribute__ ((aligned(16))) double2 {
	double x, y;
};

extern inline double2 make_double2(double x, double y)
{
	double2 d;
	d.x = x;
	d.y = y;
	return d;
}


//Memory Resources
struct Resources {
	Resources() : mem_used(0), max_mem_used(0) {}

	//Memory Allocated (in bytes)
	size_t mem_used;
	size_t max_mem_used;
};

//RGG Structure
struct RGG {
	std::vector< std::vector<int> > adj;

	void resize(int p) {
		adj.resize(p, std::vector<int>(p, 0));
	}
	
	void clearADJMAT(int N){
		adj.clear();
		resize(N);
	}
};

//Abstract class for coordinates in physical space
struct Coordinates {
	Coordinates(int _ndim) : ndim(_ndim), zero(0.0), null_ptr(NULL) {
		points = new double*[_ndim];
	};
	virtual ~Coordinates() { delete [] this->points; this->points = NULL; }

	int getDim() { return ndim; }
	bool isNull() { return points == NULL; }

	//These functions should not be accessed through this structure.  They
	//should be accessed through inherited structures to avoid SLICING.
	//These virtual definitions are used to indicate bugs in the code,
	//usually when a structure is passed by value instead of by reference

	virtual double & x(unsigned int idx) { return zero; }
	virtual double & y(unsigned int idx) { return zero; }

	virtual double *& x(void) { return null_ptr; }
	virtual double *& y(void) { return null_ptr; }

	virtual double2 getDouble2(unsigned int idx) { return make_double2(0.0, 0.0); }
	virtual void setDouble2(double2 val, unsigned int idx) {}

protected:
	double **points;

private:
	int ndim;
	double zero;
	double *null_ptr;
};

//2-Dimensional Coordinates
struct Coordinates2D : Coordinates {
	Coordinates2D() : Coordinates(2) {
		this->points[0] = NULL;
		this->points[1] = NULL;
	}

	double & x(unsigned int idx) { return this->points[0][idx]; }
	double & y(unsigned int idx) { return this->points[1][idx]; }

	double *& x() { return this->points[0]; }
	double *& y() { return this->points[1]; }

	//Compressed Values
	double2 getDouble2(unsigned int idx) {
		double2 d;
		d.x = this->points[0][idx];
		d.y = this->points[1][idx];
		return d;
	}

	void setDouble2(double2 val, unsigned int idx) {
		this->points[0][idx] = val.x;
		this->points[1][idx] = val.y;
	};
};

//Contains minimal information about system of bodies
struct Bodies {
	Bodies() : id(NULL), position(NULL), not_isolated(NULL), slot_interaction(NULL), rela(NULL), move(NULL), theta(NULL), active(NULL) {}

	uint64_t *id;			//Unique node id

	Coordinates *position;		//Spatial position
	Coordinates *F;		//Total Force on X and Y
	
	double *r; 			//Activation probability
	bool *active;
	bool *move;
	bool *not_isolated;
	bool *slot_interaction;
	double *rela;

	double *theta;
};

//System properties
struct Properties {
	Properties() : N(10), L(10), slots(10), mu2(0.5), graphID(0), rgg(), v(1.0), d(1.0), mu1(0.1), F0(1.0), p(0.0), warmup_time(0), dump(false), warmup(false), file_provided(false), p_flag(false), input_file("") {}

	int N;		//Number of agents
	int L;
	int slots;		//Number of evolution steps
	int warmup_time;
	
	double mu2;		//Escaping Probability exponent
	double v;
	double d;		//Interaction radius
	double mu1;
	double F0;
	double p;

	bool warmup;
	bool dump;
	bool file_provided;
	bool p_flag;
	
	std::string input_file; 
	
	RGG rgg; 				//RGG parameters

	int graphID;		//Unique simulation ID
};

//The Bodies System
struct BodiesSystem {
	BodiesSystem() : bodies(Bodies()), props(Properties()) {}
	BodiesSystem(Properties _p) : bodies(Bodies()), props(_p) {}

	Bodies bodies;		//Current system
	Properties props;		//System properties
};

//Custom exception class used in this program
class DynamicsException : public std::exception
{
public:
	DynamicsException() : msg("Unknown Error!") {}
	explicit DynamicsException(char const * _msg) : msg(_msg) {}
	virtual ~DynamicsException() throw () {}
	virtual const char * what() const throw () { return msg; }

protected:
	char const * msg;
};

Properties parseArgs(int argc, char **argv);

bool initializeSystem(BodiesSystem * const system);

bool simulateSystem(BodiesSystem * const system);

void destroySystem(BodiesSystem * const system);
