#include "SimilarityForces.h"

bool safe_malloc(void **data, const size_t nb);

void safe_free(void **data, const size_t nb);

void openEdgesFile();

void closeEdgesFile();

void dumpPositions(const Bodies &b, const int &N);

void makeSnapshot(const Bodies &b, const int &N, const int &time, const RGG rgg);

bool testProximity(double x_i, double y_i, double x_j, double y_j, double d);

double s1_distance(double theta_i, double theta_j, const int N);

double bonding_force(double theta_i, double theta_j, double mu2, const int N);

void initializeEdges(Bodies &bodies, const int N, const double d, RGG &rgg, const double mu2);

void findEdges(Bodies &bodies, const int N, const double d, RGG &rgg, int &active_links, int &interacting_nodes, const double mu2);

void updateForces(Bodies &bodies, RGG &rgg, const int N, const double F0, const double mu1);

std::pair<double, double> pairwiseForce(const double x_i, const double x_j, const double y_i, const double y_j, double R_dtheta, const double F0, const double mu1);
