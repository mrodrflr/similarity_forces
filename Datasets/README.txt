
Processed real-world datasets from www.sociopatterns.com used in "Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks, M. A. Rodriguez Flores, F. Papadopoulos, Physical Review Letters, Vol. 121, Issue 25, December 2018 (https://doi.org/10.1103/PhysRevLett.121.258301, arxiv: https://arxiv.org/abs/1808.00750)":

	1) Hospital Ward in Lyon. Reference: P. Vanhems et al., Estimating Potential Infection Transmission Routes in Hospital Wards Using Wearable Proximity Sensors, PLoS ONE 8(9): e73970 (2013). URL: http://www.sociopatterns.org/datasets/hospital-ward-dynamic-contact-network/

	2) Primary School in Lyon. Reference: High-Resolution Measurements of Face-to-Face Contact Patterns in a Primary School, PLOS ONE 6(8): e23176 (2011). URL: http://www.sociopatterns.org/datasets/primary-school-temporal-network-data/

	3) High School in Marseilles. Reference: R. Mastrandrea, J. Fournet, A. Barrat,
Contact patterns in a high school: a comparison between data collected using wearable sensors, contact diaries and friendship surveys. PLoS ONE 10(9): e0136497 (2015) URL: http://www.sociopatterns.org/datasets/high-school-contact-and-friendship-networks/

	4) Hypertext 2009 Conference in Turin. Reference: L. Isella et al.,  What�s in a crowd? Analysis of face-to-face behavioral networks, Journal of Theoretical Biology 271, 166 (2011). URL: http://www.sociopatterns.org/datasets/hypertext-2009-dynamic-contact-network/

The datasets have been processed in order to have the same timestamp and node ID format.

I) Dataset file names

	1) The Hospital dataset file name is "remapped_edgelist_Hospital.txt"
	2) The Primary School dataset file name is "remapped_edgelist_Primary.txt"
	3) The High School dataset file name is "remapped_edgelist_HS2013.txt"
	4) The Conference dataset file name is "remapped_edgelist_HyT2009.txt"

II) Format of the datasets
	3 Columns:	timestamp node1_ID node2_ID
	Example:	1 3 4	(At timestamp 1, Node 3 is interacting with Node 4).

III) Remapping of timestamps:
	From the original datasets we considered the following periods and remapped the original timestamps to a new range as described below:

		1) In the Hospital there are 5 observation days. We take the first 1100 time slots from days 2 to 5, which correspond to the morning shift periods. This gives a total of T = 4400 time slots, for which we remapped the timestamps to the range 1 to 4400.

		2) In the Primary School we consider the full duration of the school day in the two days of observation. This gives 1555 time slots for the first day and 1545 slots for the second. In total there are T = 3100 time slots, for which the timestamps have been remapped to the range 1 to 3100.

		3) In the High School we identify and consider all the time slots from the first recorded interaction until the last recorded interaction in each day (5 days total). In the first day there are 899 time slots, while in the rest of the days there are 1619 time slots in each day. In total we have T = 7375 time slots that were remapped to the range 1 to 7375.

		4) In the Conference dataset we have followed the same strategy as in the High School dataset. Here there are 2874 time slots in the first day, 2210 slots in the second, and 1946 slots in the third day. In total there are T = 7030 time slots.

IV) Node IDs:
	In each dataset the ID's of the N nodes have been mapped to new ID's from 0 to N-1. For each dataset we provide a file that shows the new ID assigned to each original node ID.
		1) In the Hospital dataset nodes have ID's from 0 to 69. See file "node_IDs_Hosital.txt"
		2) In the Primary School the node ID's are from 0 to 241. See file "node_IDs_Primary.txt"
		3) In the High School the node ID's are from 0 to 326. See file "node_IDs_HS2013.txt"
		4) In the Conference the node ID's are from 0 to 112. See file "node_IDs_HyT2009.txt"

See https://arxiv.org/abs/1808.00750 for further details.
