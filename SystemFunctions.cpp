#include "SystemFunctions.h"
#include <random>
#include <vector>
#include <ncurses.h>

//Random Number generators for simulation of slots
std::random_device seeder;
std::mt19937 rng(seeder());
std::uniform_real_distribution<double> p_mobility(0.0, 1.0);
std::uniform_real_distribution<double> p_activity(0.0, 1.0);
std::uniform_real_distribution<double> p_angle(0.0, 2*M_PI);

int warmup_clock = 1;

bool allocateMemory(Bodies &b, const int n)
{

	//Allocate Memory for Particles and Workspace
	if (!safe_malloc((void**)&b.id, sizeof(uint64_t) * n)) return false;
	if (!safe_malloc((void**)&b.r, sizeof(double) * n)) return false;
	if (!safe_malloc((void**)&b.rela, sizeof(double) * n)) return false;
	if (!safe_malloc((void**)&b.move, sizeof(bool) * n)) return false;
	if (!safe_malloc((void**)&b.not_isolated, sizeof(bool) * n)) return false;
	if (!safe_malloc((void**)&b.slot_interaction, sizeof(bool) * n)) return false;
	if (!safe_malloc((void**)&b.theta, sizeof(double) * n)) return false;
	if (!safe_malloc((void**)&b.active, sizeof(bool) * n)) return false;
	
	b.position = new Coordinates2D();
	b.F = new Coordinates2D();
	if (!safe_malloc((void**)&b.position->x(), sizeof(double) * n)) return false;
	if (!safe_malloc((void**)&b.F->x(), sizeof(double) * n)) return false;
		
	if (!safe_malloc((void**)&b.position->y(), sizeof(double) * n)) return false;
	if (!safe_malloc((void**)&b.F->y(), sizeof(double) * n)) return false;

	return true;
}

void releaseMemory(Bodies &b, const int n)
{
	endwin();
	safe_free((void**)&b.id, sizeof(uint64_t) * n);
	safe_free((void**)&b.position->x(), sizeof(double) * n);
	safe_free((void**)&b.F->x(), sizeof(double) * n);
	safe_free((void**)&b.position->y(), sizeof(double) * n);
	safe_free((void**)&b.F->y(), sizeof(double) * n);
	safe_free((void**)&b.r, sizeof(double) * n);
	safe_free((void**)&b.rela, sizeof(double) * n);
	safe_free((void**)&b.not_isolated, sizeof(bool) * n);
	safe_free((void**)&b.theta, sizeof(double) * n);
	safe_free((void**)&b.active, sizeof(bool) * n);
	safe_free((void**)&b.move, sizeof(bool) * n);
	safe_free((void**)&b.slot_interaction, sizeof(bool) * n);
}


void initializeAgents(Bodies &b, const int N, const int L, const double d, const double mu2, RGG &rgg, const double mu1, const bool input, const bool dump, const bool warmup, const bool p_flag, std::string file, const double p)
{
	initscr();
	std::random_device seeder;
	std::mt19937 rng(seeder());
	std::uniform_real_distribution<double> pos_uni(0.0, L);
	std::uniform_real_distribution<double> act_dist(0.0, 1.0);
	std::uniform_real_distribution<double> get_angle(0.0, 2*M_PI);
	std::vector<int> non_active;
	
	std::ifstream inf(file.c_str());
	if(input && !inf.is_open()){
		mvprintw(0, 0, "ERROR. Could not open input file. Agents will be distributed randomly throughout the space. Press any key to continue.");
		refresh();
		getchar();
		clear();
	}

	refresh();
	mvprintw(0, 0, "Placing Agents in interaction space...");
	refresh();
	mvprintw(1, 0, "*******************************************************************************");
	refresh();
	for(int i = 0; i < N; i++){
		double x, y, theta;
		x = pos_uni(rng);
		y = pos_uni(rng);
		theta = get_angle(rng);
		
		double input[3];
		if(inf.is_open()){
			std::string line;
			if(getline(inf, line)){
				int count_words = 0;
				std::string word;
				std::istringstream ss(line);
				while(ss >> input[count_words]){
					count_words++;
				}
				if ((input[0] >= 0 && input[0] <= L) || (input[1] >= 0 && input[1] <= L)){
					x = input[0];
					y = input[1];
				}
				else{
					move(2, 0);
					clrtoeol();
					move(3, 0);
					clrtoeol();
					mvprintw(2, 0, "WARNING. Coordinates found in input file for agent %d are out of the bounding box. Random coordinates will be assigned. Press any key to continue.", i);
					refresh();
					getchar();
				}
				if(count_words < 3){
					move(2, 0);
					clrtoeol();
					move(3, 0);
					clrtoeol();
					mvprintw(2, 0, "WARNING. Angular coordinate not found in input file. A radom angular coordinate will be assigned to agent %d. Press any key to continue.", i);
					refresh();
					getchar();
				}
				
			}
			else{
				move(2, 0);
				clrtoeol();
				move(3, 0);
					clrtoeol();
				mvprintw(2, 0, "WARNING. No more data found in input file. Agent %d will be placed at a random position. Press any key to continue.", i); 
				refresh();
				getchar();
			}
		}
		
		b.position->x(i) = x;
		b.position->y(i) = y;
		
		//Activation Probability
		if(p_flag)
			b.r[i] = p;
		else
			b.r[i] = act_dist(rng);
		
		//S1 theta coordinate
		b.theta[i] = theta;
		
		b.rela[i] = 0;
		b.not_isolated[i] = false;
		
		b.move[i] = false;
		b.active[i] = true;
		non_active.push_back(i);

		move(2, 0);
		clrtoeol();
		mvprintw(2, 0, "Agent %d placed on X: %f, Y: %f. S1 angular coordinate: %f.", i, x, y, theta);
		refresh();
	}
	
	initializeEdges(b, N, d, rgg, mu2);
	
	if(!warmup && dump){
		dumpPositions(b, N);
	}
	clear();
}

std::pair<double, double> simulateSlot(Bodies &bodies, const int N, const int L, const int d, RGG &rgg, const double F0, const bool dump, bool &warmup, const double mu2, const double mu1, const double v, const int warmup_time, const int t)
{
	int active_nodes = 0;
	int interacting_nodes = 0;
	int active_links = 0;
	
	int f_moved = 0;
	double avg_displacement = 0;
	
	//1) ACTIVATION MECHANISM	
	std::vector<int> non_active;
	for (int j = 0; j < N; j++){
		//Nodes that are not interacting (isolated) are assumned inactive
		if(bodies.active[j] && !bodies.not_isolated[j])
			bodies.active[j] = false;
		if(!bodies.active[j])//Put all inactive agents in a set of activation candidates 
			non_active.push_back(j);	
	}	
	
	//Activating agents
	for (int j = 0; j < non_active.size(); j++){
		int node = non_active[j];
		double p_a = p_activity(rng);
		if(p_a <= bodies.r[node])
			bodies.active[node] = true;
	}
	
	//Find total attractive force (sum of pair-wise attractive forces) for each active agent
	updateForces(bodies, rgg, N, F0, mu1);
	
	for (int i = 0; i < N; i++){
		//RESET FLAGS
		bodies.move[i] = false;
		bodies.not_isolated[i] = false;
		bodies.slot_interaction[i] = false;

		//2) ESCAPING MECHANISM
		if(bodies.active[i]){
			active_nodes++;
			double p = p_mobility(rng);
			if(p < (1 - bodies.rela[i]))
				bodies.move[i] = true;
			
			bodies.rela[i] = 0; //reset the average bonding forces for the next slot (it will be recalculated at the end of the slot given the new positions)
		}

		//3) MOTION MECHANISM
		if(bodies.move[i]){//Only activated agents and agents that escaped have a true move flag

			f_moved++; //Count moving agents
			double angle = p_angle(rng); //Random direction for random component
			double F_xi = bodies.F->x(i); //Forces on X
			double F_yi = bodies.F->y(i); //Forces on Y
			double R_xi = v*cos(angle); //Random displacement on X
			double R_yi = v*sin(angle); //Random displacement on Y
			
			double velocity_x = (F_xi + R_xi);
			double velocity_y = (F_yi + R_yi); 

			double new_pos_x = bodies.position->x(i) + velocity_x;
			double new_pos_y = bodies.position->y(i) + velocity_y;
			
			//REFLECTIVE BOUNDARY CONDITIONS
			if(new_pos_x > L || new_pos_x < 0){
				velocity_x *= -1;
			}
			if(new_pos_y > L || new_pos_y < 0){
				velocity_y *= -1;
			}
			
			double old_x = bodies.position->x(i);
			double old_y = bodies.position->y(i);
			bodies.position->x(i) = bodies.position->x(i) + velocity_x;
			bodies.position->y(i) = bodies.position->y(i) + velocity_y;
			
			//Displacement from previous slot
			double displacement = sqrt(pow(bodies.position->x(i) - old_x, 2) + pow(bodies.position->y(i) - old_y, 2));
			avg_displacement += displacement;
		}
	}
	//3) Connect agents within interaction distance
	findEdges(bodies, N, d, rgg, active_links, interacting_nodes, mu2);
	
	if(f_moved != 0){
		avg_displacement = avg_displacement / f_moved;
	}
	else{
		avg_displacement = 0;
	}


	if(warmup){
		if(warmup_clock <= warmup_time){
			warmup_clock++;
			mvprintw(0, 0, "Warmup period");
			mvprintw(1, 0, "*******************************************************************************");
			mvprintw(2, 0, "warmup time: %d/%d", warmup_clock, warmup_time);
			refresh();
		}
		else{
			warmup = false;
			resetSystem(rgg, N);
			initializeEdges(bodies, N, d, rgg, mu2);
			if(dump)
				dumpPositions(bodies, N);
			clear();
				
		}
	}	
	if(!warmup){
		mvprintw(0, 0, "Simulation time");
		mvprintw(1, 0, "*******************************************************************************");
		mvprintw(2, 0, "Slot: %d", t);
		move(3, 0);
		clrtoeol();
		mvprintw(3, 0, "Active Nodes: %d Interacting Nodes: %d Active Links: %d", active_nodes, interacting_nodes, active_links);
		move(4, 0);
		clrtoeol();
		mvprintw(4, 0, "Nodes Moved: %d", f_moved);
		move(5, 0);
		clrtoeol();
		mvprintw(5, 0, "Average Node displacement: %f", avg_displacement);
		refresh();
	}
	
	return std::make_pair(interacting_nodes, avg_displacement); 

}


void resetSystem(RGG &rgg, const int N){
	rgg.clearADJMAT(N);
}

