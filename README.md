# FDM simulator, datasets and plotting scripts

This repository contains the FDM simulator and the datasets from the paper: "Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks", M. A. Rodriguez Flores, F. Papadopoulos, Physical Review Letters, Vol. 121, Issue 25, December 2018 (https://doi.org/10.1103/PhysRevLett.121.258301, arxiv: https://arxiv.org/abs/1808.00750).
Additionally, we include two python scripts that compute and plot the temporal network properties investigated in the paper.

## FDM simulator
+ **Source files: ** SimilarityForces.cpp, SimilarityForces.h, Subroutines.cpp, Subroutines.h, SystemFunctions.cpp, SystemFunctions.h

+ **Requirements: ** 

1. The program can be compiled in Linux or Mac OS X

2. The program uses the *ncurses library* for screen output.

	installing the ncurses library in Linux:

			sudo apt-get install libncurses-dev
			
	or in Ubuntu:
	
			sudo apt-get install libncurses5-dev libncursesw5-dev
			
	installing ncurses in mac os x:
	
    		brew install homebrew/dupes/ncurses
			
3. The program uses the vector, map and random libraries from C++11
			
			use gcc version 4.7 and above to compile the program
			
+ **Compilation: **

			gcc *.cpp -std=c++11 -lncurses -o fdm.out
	or
	
		
			gcc *.cpp -std=c++0x -lncurses -o fdm.out
			
+ **Running the program: **

1. Command line arguments:

	1. -N Number of Nodes (> 0)
	2. -L Size of Euclidean Space (L x L) (> 0)
	3. -T (--slots) Number of time slots to simulate (> 0)
	4. -e (--mu_1) Exponent for exponential decay of bonding forces (> 0)
	5. -m (--mu_2) Exponent for exponential decay of attractive forces (> 0)
	6. -F (--f_zero) Force magnitude (>= 0)
	7. -w (--warmup) Warmup period duration in time slots (>= 0)
	8. -p (--probability) Assigns the same activation probability to all nodes (0 < p <= 1). If omitted, an activation probability is sampled uniformly at random for each node.
	9. -i (--input) Input file to preset the initial coordinates in the Euclidean space and in the similarity space.
	10. --dump Creates an output file named "initial_positions.txt": If there is no warmup period, it contains the initial positions of nodes in the Euclidean space. If there is a warmup period, then it contains the positions of nodes in the Euclidean space in the slot immediately after the warmup period. In both cases the file also contains the corresponding similarity coordinates of nodes.
	11. -h (--help) Displays this information
	
	For details on tuning the parameters, please see Sec. IV in the Supplemental Material of the paper, or in Appendix D of the arxiv version of the paper.

	
2. Command to run the program:

		./fdm.out -N 70 -L 95 -T 4400 -e 0.8 -m 0.9 -F 0.12 -w 2500
		
3.  Output:
	
		1. "temporal_edges.txt". This file contains the timestamped interactions between nodes, that formed in all the time slots. Each line has the format: "timestamp" "node1_ID" "node2_ID". Timestamps go from 1 to T and node ID's from 0 to N-1.
		
		2. "initial_positions.txt". If the --dump argument is provided this output file contains the positions of all nodes in the Euclidean space (either at the beginning or after a warmup period) and their coordinates in the similarity space. Each line has the format: "x-coordinate" "y-coordinate" "angular-coordinate"
		
		3. snapshots. The program outputs individual files for the node positions and the links between them in each time slot t in the folder "snapshots". The files are named "snapshot_positions_t.dat" for the positions in the Euclidean space and "snapshot_edges_t.edg" for the links observed at time t.

##Datasets

 The datasets as used in the paper are provided in the folder "Datasets"

 For more information please check the README file inside the folder.

##Scripts
 
 The scripts included in the folder "Scripts" are the following:
 
	1) properties_plotter.py: computes and plots 8 properties into a single plot file (contact duration, time beetween consecutive contacts, weight, strength and component size ditributions, as well as node strength as a function of node degree and average recurrent components as a function of number of interactions).

	2) time_paths.py: computes and plots the distribution of the shortest time-respecting paths. 

 Please check the README files inside each script's folder for instructions and further details.

***
Authors: Marco Antonio Rodriguez Flores (email: mj.rodriguezflores@edu.cut.ac.cy) and Fragkiskos Papadopoulos
