#include "Subroutines.h"

bool allocateMemory(Bodies &b, const int n);

void releaseMemory(Bodies &b, const int n);

void initializeAgents(Bodies &b, const int N, const int L, const double d, const double mu2, RGG &rgg, const double mu1, const bool input, const bool dump, const bool warmup, const bool p_flag, std::string file, const double p);

std::pair<double, double> simulateSlot(Bodies &bodies, const int N, const int L, const int d, RGG &rgg, const double F0, const bool dump, bool &warmup, const double mu2, const double mu1, const double v, const int warmup_time, const int t);

void resetSystem(RGG &rgg, const int N);
