Description:
	This script plots the following 8 properties of human contact networks as subplots of a plot with 2 rows and 4 columns:
	1) Contact duration distribution - log binned (subplot a)
	2) Time between consecutive contacts distribution (subplot b)
	3) Weight distribution in the aggregated network of contacts (subplot c)
	4) Strength distribution in the aggregated network of contacts (subplot d)
	5) Component size distribution - without binning (subplot e)
	6) Node degree VS Node strength in the aggregated network of contacts (subplot f)
	7) Group duration as a function of its size (subplot g)
	8) Average recurrent components per interval as a function of number of interactions (subplot h)

Requirements:
	python 2.7
	matploblib
	numpy
	natsort library: https://pypi.org/project/natsort/ Please follow the instructions to set it up.

1) Usage:

	python properties_plotter.py -h to see the help menu.
	
	python properties_plotter.py -o OUTPUT_NAME -l LEGEND -f INPUT_DIRECTORY -m MINUTES -d DT
	
	example: python properties_plotter.py -o FDM_Hospital -l "Force-dir. Motion" -f "input hospital" -m 10 -d 20
	
	See section 2 of this readme for more details about the argument values.

2) Required arguments:

-o OUTPUT_NAME / --output OUTPUT_NAME : The output plot name sufix to add to all output files and plots : Type expected is 'string'
		The output plot produced will be named Plot_Properties_[OUTPUT_NAME].pdf

-l LEGEND / --legend LEGEND  : The legend to use for simulation results in the plots	  : Type expected is 'string'
		The text to use for simulation results in the plots' legend box.
	
-f INPUT_DIRECTORY / --folder INPUT_DIRECTORY  : The directory name or path where the input files are located  : Type expected is 'string'
		The name of the folder that contains the input files.
		The script will read all the files in the folder with .txt extension.
		The dataset edgelists currently recognized must be named: remapped_edgelist_Hospital.txt, remapped_edgelist_Primary.txt, remapped_edgelist_HS2013.txt and remapped_edgelist_HyT2009.txt.
		For new datasets not supported please include the keyword 'DATASET' or 'dataset' in the file name.
		The rest of the files in the folder not recognized as datasets will be treated as simulation files.

-m MINUTES / --minutes MINUTES : The bin size in minutes to find recurrent components	  : Type expected is 'float'
		The size of the bin in minutes determines the period by which recurrent components are counted.
		For example MINUTES = 10 means that a component will be counted once in every interval of 10 minutes.

-d DT / --dt DT	     : The time slot duration/Dataset resolution in seconds	  : Type expected is 'int'
		The resolution of the dataset. How many seconds each slot represents. For example if the timestamps of the input networks are numerated from 1 to T, and each timestamp represents 20 seconds then DT must be set to 20 in order to properly compute time-respecting properties like detecting components in time intervals. On the other hand if the time stamps in the network files represent real seconds then use MINUTES = 1

3) Optional arguments:

--patterns VALUE	     : Optional argument to print the recurrent component patterns for each input file.
				>If VALUE is 1 or bigger, the patterns will be printed.
				>If this argument is not provided VALUE is assumed to be 0 and the patterns are not printed.
				>Type expected is 'int'

4) Output:
	The script creates 3 folders inside the input folder given:
		1) Plots: it contains the output plot of properties.
		2) Raw_Files: it contains the raw files of the properties computed. For each property a file is generated for the dataset and another file for the averages of simulations. The format of the files is: column 1 is the value on the x-axis and column 2 is the value on the y-axis. This folder also contains a readme.txt file that contains the metrics information printed in the console.
		3) Raw_Samples: it contains the raw files that contain only a list of raw samples for the following properties: contact durations, time between consecutive contacts, group size, node strenght and link weight. A file is generated for each input network individually.

--------------------------------------------------------------------------------------------------
Authors: Marco Antonio Rodriguez Flores & Fragkiskos Papadopoulos
Publication: Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks
doi: 10.1103/PhysRevLett.121.258301
