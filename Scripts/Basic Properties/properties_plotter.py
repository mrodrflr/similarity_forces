# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 14:44:10 2016

@author: Marco
"""

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt
from matplotlib import gridspec
from matplotlib.patches import Rectangle
from matplotlib.ticker import AutoMinorLocator
import numpy as np
import os
import math
import glob, sys
import argparse
from natsort import natsort

#Logarithmic binning
def func_log_bin(qini, pdf_dict, numeropuntos=30):
    samples = sorted(pdf_dict.keys())

    nmin = min(samples)
    nmax = max(samples)

    binned_pdf = dict()

    for i in range(0, nmax+1):
        if i < qini:
            if i in pdf_dict:
                binned_pdf[i] = pdf_dict[i]
                continue

    qexp = 0.0000001+(float(nmax)/qini)**(1./float(numeropuntos))

    xini = qini
    for i in range(2, numeropuntos+1):
        xfin = qini*qexp**(i-1)

        qnorm = 0.
        qpos = 0.
        for j in range(0, nmax):
            if j >= xini and j < xfin:
                if j in pdf_dict:
                    p = pdf_dict[j]
                else:
                    p = 0
                qnorm = qnorm + p
                qpos = qpos + float(j)*p
        if qnorm > 0:
            binned_pdf[qpos/qnorm] = qnorm/(xfin - xini)

        xini = xfin

    return binned_pdf

#Plotting function
def plot_data(data_dict, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, ax):
    if (x_scale != "log" and x_scale != "linear") or (y_scale != "log" and y_scale != "linear"):
        print("Aborting. Wrong scale type for plot")
        exit()

    sim_runs = 0
    log_bins = 20

    min_x = min_y = 999999
    max_x = max_y = 0

    print("Printing "+str(x_label)+" plot...")
    ax.set_xscale(x_scale)
    ax.set_yscale(y_scale)
    ax.set_xlabel(x_label, fontsize=22)
    ax.set_ylabel(y_label, fontsize=22)
    ax.tick_params(which='both', labelsize=15)
    if x_scale == "linear":
        minor_xs = AutoMinorLocator(10)
        ax.xaxis.set_minor_locator(minor_xs)
    if y_scale == "linear":
        minor_ys = AutoMinorLocator(10)
        ax.yaxis.set_minor_locator(minor_ys)
    labels = []
    labels_order = []

    sim_data = dict()
    raw_data = dict()
    for i in data_dict:

        if plot_type == "PDF":
            samples = np.array(sorted(data_dict[i]))
            uniques, counts = np.unique(samples, return_counts = True)
            total = sum(counts)
            counts = counts/float(total)
            pdf = dict(zip(uniques, counts))

        elif plot_type == "FUNCTION":
            for value in data_dict[i]:
                data_dict[i][value] = np.mean(data_dict[i][value])

        if plot_type != "FUNCTION":
            raw_data[i] = pdf

            if type_list[i] == "dataset":
                binned_dataset = dict()

                if binning == "log":
                    pdf = func_log_bin(1.1, pdf, log_bins)
                    binned_dataset = pdf
                elif binning == "nplog":
                    hist = np.histogram(pdf.keys(), weights=pdf.values(), bins=np.logspace(np.log10(min(pdf.keys())), np.log10(max(pdf.keys())), log_bins), density=True)
                    pdf = dict(zip(hist[1], hist[0]))
                    x_values = pdf.keys()
                    for x in x_values:
                        if pdf[x] <= 0:
                            del pdf[x]

                    binned_dataset = pdf
                elif binning == "linear":
                    hist = np.histogram(samples, bins=10, density=True)
                    pdf = dict(zip(hist[1], hist[0]))
                    binned_dataset = pdf
                else:
                    binned_dataset = pdf

                if min(binned_dataset.keys()) < min_x:
                    min_x = min(binned_dataset.keys())
                if max(binned_dataset.keys()) > max_x:
                    max_x = max(binned_dataset.keys())
                if min(binned_dataset.values()) < min_y:
                    min_y = min(binned_dataset.values())
                if max(binned_dataset.values()) > max_y:
                    max_y = max(binned_dataset.values())

                file_string = x_label+"_"+y_label+"_"+legends[i]
                file_string = file_string.replace(" ", "_")
                fcont = open(files_path+"/RAW_"+file_string+".txt", "w")

                sort_bs_dat = sorted(binned_dataset.keys())
                sort_ys_dat = []
                for b in sort_bs_dat:
                    fcont.write(str(b)+" "+str(binned_dataset[b])+"\n")
                    sort_ys_dat.append(pdf[b])

                fcont.close()

                label = ax.scatter(sort_bs_dat, sort_ys_dat, marker='s', s=350, edgecolors='black', label=legends[i], linewidth=1, facecolors='blue', zorder=1, clip_on=False)
                labels.append(label)
                labels_order.append(0)
            elif type_list[i] == "simulation":
                sim_runs += 1
                for j in raw_data[i]:
                    if not j in sim_data:
                        sim_data[j] = []
                    sim_data[j].append(raw_data[i][j])

        elif plot_type == "FUNCTION":
            raw_data[i] = data_dict[i]

            copy_keys = data_dict[i].keys()
            for d in copy_keys:
                if raw_data[i][d] == 0:
                    del raw_data[i][d]
                    continue

            if type_list[i] == "dataset":
                binned_dataset = dict()

                if binning == "log":
                    copy_keys = raw_data[i].keys()
                    for d in copy_keys:
                        log_bin = round(math.log(d)/math.log(2))
                        #log_bin = 2**log_bin
                        log_bin = (2**log_bin + 2**(log_bin-1))/2.0
                        if not log_bin in binned_dataset:
                            binned_dataset[log_bin] = []

                        binned_dataset[log_bin].append(data_dict[i][d])

                    for d in  binned_dataset:
                        binned_dataset[d] = np.mean(binned_dataset[d])
                else:
                    binned_dataset = data_dict[i]

                if min(binned_dataset.keys()) < min_x:
                    min_x = min(binned_dataset.keys())
                if max(binned_dataset.keys()) > max_x:
                    max_x = max(binned_dataset.keys())
                if min(binned_dataset.values()) < min_y:
                    min_y = min(binned_dataset.values())
                if max(binned_dataset.values()) > max_y:
                    max_y = max(binned_dataset.values())

                file_string = x_label+"_"+y_label+"_"+legends[i]
                file_string = file_string.replace(" ", "_")
                fgat = open(files_path+"/RAW_"+file_string+".txt", "w")

                sort_bs_dat = sorted(binned_dataset.keys())
                sort_ys_dat = []
                for d in sort_bs_dat:
                    sort_ys_dat.append(binned_dataset[d])
                    fgat.write(str(d)+" "+str(binned_dataset[d])+"\n")

                fgat.close()
                label = ax.scatter(sort_bs_dat, sort_ys_dat, marker='s', s=350, edgecolors='black', label=legends[i], linewidth=1, facecolors='blue', zorder=1, clip_on=False)
                labels.append(label)
                labels_order.append(0)

            elif type_list[i] == "simulation":
                sim_runs += 1
                for d in raw_data[i]:
                    if not d in sim_data:
                        sim_data[d] = []
                    sim_data[d].append(raw_data[i][d])

    sort_x = sorted(sim_data.keys())
    file_string = x_label+"_"+y_label
    file_string = file_string.replace(" ", "_")
    fcont = open(files_path+"/RAW_"+file_string+"_averages.txt", "w")
    for s in sort_x:
        if plot_type == "ECCDF" or plot_type == "PDF":
            denominator = float(sim_runs)
        else:
            denominator = float(len(sim_data[s]))

        sim_data[s] = sum(sim_data[s])/denominator

    if plot_type != "FUNCTION":
        if binning == "log":
            binned_avg = func_log_bin(1.1, sim_data, log_bins)
        elif binning == "nplog":
            hist = np.histogram(sim_data.keys(), weights=sim_data.values(), bins=np.logspace(np.log10(min(sim_data.keys())), np.log10(max(sim_data.keys())), log_bins), density=True)
            binned_avg = dict(zip(hist[1], hist[0]))
            x_values = binned_avg.keys()
            for x in x_values:
                if binned_avg[x] <= 0:
                    del binned_avg[x]

        elif binning == "linear":
            hist = np.histogram(samples, bins=10, density=True)
            binned_avg = dict(zip(hist[1], hist[0]))
        else:
            binned_avg = sim_data
    else:
        binned_avg = dict()
        if binning == "log":
            for d in sim_data:
                log_bin = round(math.log(d)/math.log(2))
                #log_bin = 2**log_bin
                log_bin = (2**log_bin + 2**(log_bin-1))/2.0
                if not log_bin in binned_avg:
                    binned_avg[log_bin] = []
                binned_avg[log_bin].append(sim_data[d])
            for d in binned_avg:
                binned_avg[d] = np.mean(binned_avg[d])
        else:
            binned_avg = sim_data

    if (len(binned_avg) > 0):
        if min(binned_avg.keys()) < min_x:
            min_x = min(binned_avg.keys())
        if max(binned_avg.keys()) > max_x:
            max_x = max(binned_avg.keys())
        if min(binned_avg.values()) < min_y:
            min_y = min(binned_avg.values())
        if max(binned_avg.values()) > max_y:
            max_y = max(binned_avg.values())

    sort_bs = sorted(binned_avg.keys())
    sort_ys = []
    for s in sort_bs:
        fcont.write(str(s)+" "+str(binned_avg[s])+"\n")
        sort_ys.append(binned_avg[s])
    fcont.close()

    label = ax.scatter(binned_avg.keys(), binned_avg.values(), marker='o', s=250, edgecolors='black', label=legend_avgs, linewidth=1, facecolors='lime', zorder=3, clip_on=False)
    labels.append(label)
    labels_order.append(1)

    ax.set_xlim([min_x, max_x])
    ax.set_ylim([min_y, max_y])

    labels_order, labels = zip(*sorted(zip(labels_order, labels)))
    sort_labels = list(labels)
    ax.legend(handles=sort_labels, loc=legend_loc, prop={'size': 15})

#Union Find Functions
def find(u, root):
    return root[u]

def union(u, v, root):
    root_u = root[u]
    for node in root:
        if root[node] == root_u:
            root[node] = root[v]

def get_nodes(root, c):
    nodes = set()
    for node in root:
        if root[node] == c:
            nodes.add(node)

    return nodes

#finds all snapshots in a given directory
def get_file_list(dir_path):
    return natsort(glob.glob(dir_path), key = lambda y: y.lower())

def read_raw(path):
    data_array = np.genfromtxt(path, dtype="int")
    return data_array

#stores all snapshots in a list
def store_edgelists(file_list, overlap_path):
    i = 0
    runs = 0
    legends = dict()
    edgelists = []
    datesets = []
    skip = []
    cycles = []
    type_list = dict()
    for file1 in file_list:
        file_name = file1.split('/')
        file_name = file_name[-1]
        edgelists.append(read_raw(file1))
        print(file1)
        if file1 == overlap_path+"/remapped_edgelist_Hospital.txt":
            legends[i] = "Hospital"
            type_list[i] = "dataset"
        elif file1 == overlap_path+"/remapped_edgelist_Primary.txt":
            legends[i] = "Primary School"
            type_list[i] = "dataset"
        elif file1 == overlap_path+"/remapped_edgelist_HS2013.txt":
            legends[i] = "High School 2013"
            type_list[i] = "dataset"
        elif file1 == overlap_path+"/remapped_edgelist_HyT2009.txt":
            legends[i] = "Conference"
            type_list[i] = "dataset"
        elif "MIT" in file_name:
            legends[i] = "MIT Social Evolution"
            type_list[i] = "dataset"
        elif "DATASET" in file_name or "dataset" in file_name:
            legends[i] = "Dataset"
            type_list[i] = "dataset"
        else:
            type_list[i] = "simulation"
            legends[i] = str(i+1)
            runs += 1
        i += 1

    return edgelists, legends, type_list, runs, skip, cycles

#file naming
parser = argparse.ArgumentParser(description="Properties Plotter for Human Contact Networks", epilog="Authors: Marco Antonio Rodriguez Flores and Fragkiskos Papadopoulos\nPublication: Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks (10.1103/PhysRevLett.121.258301)")
parser.add_argument('-o', '--output', help='Output Plot Name Suffix', required=True)
parser.add_argument('-l', '--legend', help='Legend for simulation averages in the plot', required=True)
parser.add_argument('-m', '--minutes', help='Size of recurrent components bins in minutes', required=True, type=float)
parser.add_argument('-f', '--folder', help='Folder name/path that contains the input files', required=True)
parser.add_argument('-d', '--dt', help='Time slot duration', required=True, type=int)
parser.add_argument('--patterns', help='Print recurrent component patterns for each input file. Use --patterns 1 for Yes. If the argument is not provided the patterns will not be printed.', type =int, default=0)
args =parser.parse_args()

file_sufix = args.output
legend_avgs = args.legend
minutes = args.minutes
overlap_path = args.folder
rc_f = args.patterns
if rc_f > 0:
    print_patterns = True
else:
    print_patterns = False

raw_samples = True
dt = args.dt

files_path = os.path.join(overlap_path, "Raw_Files")
graphs_path = os.path.join(overlap_path, "Plots")
samples_path = os.path.join(overlap_path, "Raw_Samples")

if not os.path.exists(overlap_path):
    print(overlap_path+" folder not found! Create the folder and place the edgelists to process in it.")
    exit()

if not os.path.exists(files_path):
    os.makedirs(files_path)

if not os.path.exists(graphs_path):
    os.makedirs(graphs_path)

if not os.path.exists(samples_path):
    os.makedirs(samples_path)

edgelists, legends, type_list, runs, skips, cycles = store_edgelists(get_file_list(overlap_path+'/*.txt'), overlap_path) #Reads all temporal edgelists in the overlap folder

k_vs_sk = dict()

strength_samples = dict()
weight_samples = dict()
kT_samples = dict()

durations_list = dict()
gaps_list = dict()
slot_interacting = dict()
node_interactions = dict()
interacting_dist = dict()
interactions_list = dict()
interactions_rec = dict()
interactions_tot = dict()
size_duration = dict()
g_durations = dict()
node_tot = dict()
node_rec = dict()
rec_per_node = dict()
cycle_unis = dict()
cycle_tots = dict()
cycle_avg = dict()
total_comp_sizes = dict()
comp_dist = dict()
links_slots = dict()
num_nodes = dict()
el_id = 0
sim_id = 0

for el in edgelists:
    slots = el[:,0]
    nodes_i = el[:,1]
    nodes_j = el[:,2]

    slots = [dt*x for x in slots]
    t0 = min(slots)
    snapshots = max(slots)

    aggregated = dict()
    k_vs_sk[el_id] = dict()

    strength_samples[el_id] = []
    weight_samples[el_id] = []
    kT_samples[el_id] = []
    rec_per_node[el_id] = []

    snp_interacting = dict()
    node_interactions = dict()
    node_list = []
    edge_timestamps = dict()
    links_slots[el_id] = dict()
    step_communities = dict()
    pair_communities = dict()
    com_members = dict()
    root = dict() #nodes with same root/id belong to the same component
    prev_slot = slots[0]
    step_communities[prev_slot] = set()
    pair_communities[prev_slot] = set()
    total_comp_sizes[el_id] = []

    if type_list[el_id] == "simulation":
        sim_id += 1

    seen_edge = dict()

    for l in range(len(slots)):

        if not slots[l] in seen_edge:
            seen_edge[slots[l]] = dict()
        mn = min(nodes_i[l], nodes_j[l])
        mx = max(nodes_i[l], nodes_j[l])
        edge_id = str(int(mn))+" "+str(int(mx))
        if edge_id in seen_edge[slots[l]]:
            continue
        else:
            seen_edge[slots[l]][edge_id] = True

        #Aggregated network
        if not nodes_i[l] in aggregated:
            aggregated[nodes_i[l]] = dict()
        if not nodes_j[l] in aggregated[nodes_i[l]]:
            aggregated[nodes_i[l]][nodes_j[l]] = 0

        if not nodes_j[l] in aggregated:
            aggregated[nodes_j[l]] = dict()
        if not nodes_i[l] in aggregated[nodes_j[l]]:
            aggregated[nodes_j[l]][nodes_i[l]] = 0

        aggregated[nodes_i[l]][nodes_j[l]] += 1
        aggregated[nodes_j[l]][nodes_i[l]] += 1

        #Find step communities in a slot
        if slots[l] != prev_slot or l == (len(slots) - 1): #changed slot, compute components of previous slot.

            if slots[l] == prev_slot:
                if not nodes_i[l] in root:
                    root[nodes_i[l]] = nodes_i[l]
                if not nodes_j[l] in root:
                    root[nodes_j[l]] = nodes_j[l]

                if find(nodes_i[l], root) != find(nodes_j[l], root):
                    union(nodes_i[l], nodes_j[l], root)

            comp_ids = np.array(list(root.values()))
            unique, counts = np.unique(comp_ids, return_counts = True)

            for c in range(len(unique)):
                if counts[c] == 1:
                    print("ERROR")
                    print(unique)
                    print(unique[c])
                    print(root)
                    exit()
                total_comp_sizes[el_id].append(counts[c])
                if counts[c] > 2: #Ignore components of size 2 or less
                    members = get_nodes(root, unique[c])
                    key = str(sorted(list(members)))
                    step_communities[prev_slot].add(key)
                    com_members[key] = members
                elif counts[c] == 2:
                    members = get_nodes(root, unique[c])
                    key = str(sorted(list(members)))
                    pair_communities[prev_slot].add(key)
                    com_members[key] = members

            prev_slot = slots[l]
            root = dict()
            if not prev_slot in step_communities:
                step_communities[prev_slot] = set()
                pair_communities[prev_slot] = set()

        #Find component of current edge
        if not nodes_i[l] in root:
            root[nodes_i[l]] = nodes_i[l]
        if not nodes_j[l] in root:
            root[nodes_j[l]] = nodes_j[l]

        if find(nodes_i[l], root) != find(nodes_j[l], root):
            union(nodes_i[l], nodes_j[l], root)

        #Timestamps per edge
        first_node = min(nodes_i[l], nodes_j[l])
        second_node = max(nodes_i[l], nodes_j[l])
        edge_key = str(int(first_node))+" "+str(int(second_node))

        if not edge_key in edge_timestamps:
            edge_timestamps[edge_key] = []

        if not slots[l] in edge_timestamps[edge_key]:
            edge_timestamps[edge_key].append(slots[l])

        #Interacting nodes per slot
        if not slots[l] in snp_interacting:
            snp_interacting[slots[l]] = []

        if not nodes_i[l] in snp_interacting[slots[l]]:
            snp_interacting[slots[l]].append(nodes_i[l])

        if not nodes_j[l] in snp_interacting[slots[l]]:
            snp_interacting[slots[l]].append(nodes_j[l])

        #Node Interactions
        if not nodes_i[l] in node_interactions:
            node_interactions[nodes_i[l]] = 0
        node_interactions[nodes_i[l]] += 1

        if not nodes_j[l] in node_interactions:
            node_interactions[nodes_j[l]] = 0
        node_interactions[nodes_j[l]] += 1

        #Links per slot
        if not slots[l] in links_slots[el_id]:
            links_slots[el_id][slots[l]] = 0
        links_slots[el_id][slots[l]] += 1

        if not nodes_i[l] in node_list:
            node_list.append(int(nodes_i[l]))

        if not nodes_j[l] in node_list:
            node_list.append(int(nodes_j[l]))

    N = len(node_list)
    print(N)
    num_nodes[el_id] = N
    node_duration = dict()

    for s in step_communities.keys():
        if len(step_communities[s]) == 0:
            step_communities.pop(s, 'None')
    for s in pair_communities.keys():
        if len(pair_communities[s]) == 0:
            pair_communities.pop(s, 'None')

    #Contacts and Gaps Computation
    if True:
        durations_list[el_id] = []
        gaps_list[el_id] = []
        edge_duration = dict()
        #Contact Durations and Gaps Computation
        for edge in edge_timestamps:

            edge_duration[edge] = 0
            contact = 0
            gap = 0
            ended = 0
            prev_t = edge_timestamps[edge][0]

            for t in edge_timestamps[edge]:
                if t > (prev_t + dt):
                    contact = edge_duration[edge]
                    durations_list[el_id].append(contact)
                    edge_duration[edge] = 1
                    gaps_list[el_id].append(int((t - prev_t)/dt))
                else:
                    edge_duration[edge] += 1

                prev_t = t

            if edge_duration[edge] != 0: #Last contact
                contact = edge_duration[edge]
                durations_list[el_id].append(contact)

    #Interactions and Interacting Nodes plots
    if True:
        slot_interacting[el_id] = dict()
        interacting_dist[el_id] = []
        for s in range(t0, snapshots+dt, dt):

            if not s in links_slots[el_id]:
                links_slots[el_id][s] = 0

            if not s in snp_interacting:
                slot_interacting[el_id][s] = 0
                interacting_dist[el_id].append(0)
            else:
                slot_interacting[el_id][s] = len(snp_interacting[s])
                interacting_dist[el_id].append(len(snp_interacting[s]))

        interactions_list[el_id] = []
        for node in node_interactions:
            interactions_list[el_id].append(node_interactions[node])

    #Interactions VS Components plots
    if True:
        interactions_rec[el_id] = dict()
        interactions_tot[el_id] = dict()
        cycle_unis[el_id] = dict()
        cycle_tots[el_id] = dict()
        cycle_avg[el_id] = dict()
        minutes_to_secs = minutes * 60.0
        snps_in_bin = round(minutes_to_secs / dt)

        s_slots = sorted(step_communities.keys())
        min_slot = min(s_slots)
        max_slot = max(s_slots)

        node_tot = dict()
        node_rec = dict()

        gatherings = dict()
        last_seen = dict()
        rec_seen = []
        bin_com = dict()
        com_count = 1;
        time_bin = 1
        snp_bin_start = s_slots[0]
        gatherings_bins = dict()

        for snp in range(int(min_slot), int(max_slot+dt), dt):

            if (snp - snp_bin_start)/dt >= snps_in_bin:
                time_bin += 1
                snp_bin_start = snp

            if not time_bin in bin_com:
                bin_com[time_bin] = []

            if not snp in step_communities:
                continue

            for c in step_communities[snp]:
                if not c in gatherings:
                    gatherings[c] = com_count
                    if not com_count in bin_com[time_bin]:
                        bin_com[time_bin].append(com_count)

                    gatherings_bins[com_count] = []
                    gatherings_bins[com_count].append(time_bin)
                    com_count += 1
                else:
                    if not gatherings[c] in bin_com[time_bin]:
                        bin_com[time_bin].append(gatherings[c])
                        #if (snp - last_seen[c]) > dt:
                        rec_seen.append(gatherings[c])
                        gatherings_bins[gatherings[c]].append(time_bin)

                last_seen[c] = snp

        max_bin = max(bin_com.keys())
        total_unique = com_count - 1
        paired_bins = max_bin - 1
        total_seen = 0
        for d1 in bin_com:
            total_seen += len(bin_com[d1])

        total_rec = total_seen - total_unique

        if paired_bins == 0:
            paired_bins = 1.0

        rec_avg = total_rec / float(paired_bins)

        cycle_unis[el_id] = total_unique
        cycle_tots[el_id] = total_seen
        cycle_avg[el_id] = rec_avg

        if print_patterns:
            bins_start = int(min_slot/60.0)
            time_bins = int(max_slot/60.0)
            fig0 = plt.figure()
            ax0 = fig0.add_subplot(1,1,1)
            plt.xlabel("Time in minutes")
            plt.ylabel("Component ID")
            ax0.set_xlim([bins_start, time_bins])
            if type_list[el_id] == "dataset":
                plt.title("Recurrent Components in "+str(legends[el_id])+" dataset")
            elif type_list[el_id] == "simulation":
                plt.title("Recurrent Components in simulation "+str(sim_id))

            extra1 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
            extra2 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
            extra3 = Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)

            print("printing recurrent component patterns for input file "+str(el_id)+"...")
            str1 = "Total = "+str(total_seen)
            str2 = "Unique = "+str(total_unique)
            str3 = "Avg = "+str(rec_avg)
            ax0.legend([extra1, extra2, extra3], (str1, str2, str3), loc="upper left")
            seen = []
            for d in bin_com:
                for community in bin_com[d]:
                    if not community in seen:
                        ax0.plot((bins_start+(d-1)*minutes, bins_start+d*minutes), (community, community), 'k-', linewidth=1)
                        seen.append(community)
                    else:
                        ax0.plot((bins_start+(d-1)*minutes, bins_start+d*minutes), (community, community), 'k-', linewidth=1, color='blue')

            if type_list[el_id] == "dataset":
                plt.savefig(graphs_path+'/Recurrent_Components_'+str(legends[el_id])+'_dataset.pdf')
            if type_list[el_id] == "simulation":
                plt.savefig(graphs_path+'/Recurrent_Components_simulation_'+str(sim_id)+'.pdf')
            plt.close(fig0)

        for node in node_interactions:
            gatherings = dict()
            last_seen = dict()
            rec_seen = []
            bin_com = dict()
            com_count = 1;
            time_bin = 1
            snp_bin_start = s_slots[0]

            if not node in node_tot:
                node_tot[node] = []
                node_rec[node] = []

            for snp in range(int(min_slot), int(max_slot+dt), dt):
                if (snp - snp_bin_start)/dt >= snps_in_bin:
                    time_bin += 1
                    snp_bin_start = snp

                if not time_bin in bin_com:
                    bin_com[time_bin] = []

                if not snp in step_communities:
                    continue

                for c in step_communities[snp]:

                    if not node in com_members[c]:
                        continue

                    if not c in gatherings:
                        gatherings[c] = com_count
                        if not com_count in bin_com[time_bin]:
                            bin_com[time_bin].append(com_count)
                        com_count += 1
                    else:
                        if not gatherings[c] in bin_com[time_bin]:
                            bin_com[time_bin].append(gatherings[c])
                            #if (snp - last_seen[c]) > dt:
                            rec_seen.append(gatherings[c])

                    last_seen[c] = snp

            node_tot[node].append(com_count-1)
            node_rec[node].append(len(rec_seen))
            rec_per_node[el_id].append(len(rec_seen))

            #Logarithmic Binning of interactions
            if not node_interactions[node] in interactions_rec[el_id]:
                interactions_rec[el_id][node_interactions[node]] = []
                interactions_tot[el_id][node_interactions[node]] = []

            interactions_rec[el_id][node_interactions[node]].append(node_rec[node])
            interactions_tot[el_id][node_interactions[node]].append(node_tot[node])

    #Aggragated results
    static_k = dict()
    node_s = dict()
    for i in aggregated:
        for j in aggregated[i]:
            weight_samples[el_id].append(aggregated[i][j])
        si = sum(aggregated[i].values())
        strength_samples[el_id].append(si)
        node_s[i] = si

    min_s = min(node_s.itervalues())
    min_s_n = [k for k in node_s if node_s[k] == min_s]
    for n in node_s:
        static_k[n] = len(aggregated[n]) #Degree in Aggregated Network
        kT_samples[el_id].append(static_k[n])
        si = node_s[n]

        if not static_k[n] in k_vs_sk[el_id]:
            k_vs_sk[el_id][static_k[n]] = []
        k_vs_sk[el_id][static_k[n]].append(si)

    #Component/Group Size Distribution
    comp_dist[el_id] = dict()
    for c_size in total_comp_sizes[el_id]:
        if not c_size in comp_dist[el_id]:
            comp_dist[el_id][c_size] = 0
        comp_dist[el_id][c_size] += 1

    #Group Size Duration
    g_durations[el_id] = []
    size_duration[el_id] = dict()
    com_stamps = dict()
    com_size = dict()
    #print("Group size VS Duration...")
    for snp in step_communities:
        #print("Processing time stamp "+str(snp)+"/"+str(snapshots))

        for c in step_communities[snp]:
            cluster = com_members[c]

            key = str(sorted(cluster))
            if not key in com_stamps:
                com_stamps[key] = []
                com_size[key] = len(cluster)
            com_stamps[key].append(snp)

    for snp in pair_communities:

        for c in pair_communities[snp]:
            cluster = com_members[c]

            key = str(sorted(cluster))
            if not key in com_stamps:
                com_stamps[key] = []
                com_size[key] = len(cluster)
            com_stamps[key].append(snp)

    for group in com_stamps:
        duration = len(com_stamps[group])
        g_durations[el_id].append(duration)

        if not com_size[group] in size_duration[el_id]:
            size_duration[el_id][com_size[group]] = []
        size_duration[el_id][com_size[group]].append(duration)

    el_id += 1

#Grid Spec
inds = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)', '(g)', '(h)']

rows = 2
cols = 4
fw = 25
fh = 8.8

fig = plt.figure(figsize=(fw,fh))
gs = gridspec.GridSpec(rows, cols)
f_grid = dict()

f = 1
for r in range(rows):
    for c in range(cols):
        f_grid[f] = plt.subplot(gs[r, c])

        f += 1

#MAIN PLOT Segment -----------------------------------------------------------------------------------------------------------------------------------

#Contact distribution
f_grid[1].set_title(inds[0], fontsize=25)

x_label = 'contact duration'
y_label = 'distribution'
x_scale = 'log'
y_scale = 'log'
plot_type = 'PDF'
binning = 'log'
legend_loc = "upper right"

plot_data(durations_list, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[1])

#Gaps distribution
f_grid[2].set_title(inds[1], fontsize=25)

x_label = 'time between consecutive contacts'
y_label = 'distribution'
x_scale = 'log'
y_scale = 'log'
plot_type = 'PDF'
binning = 'log'
legend_loc = "upper right"

plot_data(gaps_list, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[2])

#Weights Distribuion
f_grid[3].set_title(inds[2], fontsize=25)

x_label = 'weight'
y_label = 'distribution'
x_scale = 'log'
y_scale = 'log'
plot_type = 'PDF'
binning = 'log'
legend_loc = "upper right"

plot_data(weight_samples, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[3])

#Strength Distribuion
f_grid[4].set_title(inds[3], fontsize=25)

x_label = 'strength'
y_label = 'distribution'
x_scale = 'log'
y_scale = 'log'
plot_type = 'PDF'
binning = 'nplog'
legend_loc = "upper right"

plot_data(strength_samples, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[4])

#Component/Group size distribution
f_grid[5].set_title(inds[4], fontsize=25)

x_label = 'component size'
y_label = 'distribution'
x_scale = 'log'
y_scale = 'log'
plot_type = 'PDF'
binning = 'none'
legend_loc = "lower left"

plot_data(total_comp_sizes, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[5])

#k VERSUS s(k)
f_grid[6].set_title(inds[5], fontsize=25)

x_label = 'node degree'
y_label = 'average strength'
x_scale = 'log'
y_scale = 'log'
plot_type = 'FUNCTION'
binning = 'log'
legend_loc = "upper left"

plot_data(k_vs_sk, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[6])

#Group Size VS Group Duration
f_grid[7].set_title(inds[6], fontsize=25)

x_label = 'group size'
y_label = 'average total duration'
x_scale = 'linear'
y_scale = 'linear'
plot_type = 'FUNCTION'
binning = 'none'
legend_loc = "upper right"

plot_data(size_duration, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[7])

#Interactions VS Recurrent Components
f_grid[8].set_title(inds[7], fontsize=25)

x_label = 'number of node interactions'
y_label = 'ave. rec. components'
x_scale = 'log'
y_scale = 'log'
plot_type = 'FUNCTION'
binning = 'log'
legend_loc = "upper left"

data = interactions_rec.keys()
for i in data:
    b = interactions_rec[i].keys()
    no_rec = True
    for x in b:
        if np.mean(interactions_rec[i][x]) != 0:
            no_rec = False

    if no_rec:
        del interactions_rec[i]

plot_data(interactions_rec, type_list, legends, legend_loc, x_label, y_label, x_scale, y_scale, plot_type, binning, f_grid[8])

plt.tight_layout()
plt.savefig(graphs_path+'/Plot_Properties_'+file_sufix+'.pdf')#, bbox_inches='tight')
plt.close()

#-----------------------------------------------------------------------------------------------------------------------------------------------

if raw_samples:
    sim_runs = 0
    for i in type_list:
        if type_list[i] == "dataset":
            file_subname = legends[i]
            file_subname = file_subname.replace(" ", "_")
        elif type_list[i] == "simulation":
            sim_runs += 1
            file_subname = "Simulation_"+str(sim_runs)

        contacts_file = open(samples_path+"/raw_contacts_"+file_subname+".txt", "w")
        gaps_file = open(samples_path+"/raw_gaps_"+file_subname+".txt", "w")
        weights_file = open(samples_path+"/raw_weights_"+file_subname+".txt", "w")
        strengths_file = open(samples_path+"/raw_strengths_"+file_subname+".txt", "w")
        compsizes_file = open(samples_path+"/raw_group_size_"+file_subname+".txt", "w")

        for duration in durations_list[i]:
            contacts_file.write(str(duration)+"\n")
        contacts_file.close()

        for gap in gaps_list[i]:
            gaps_file.write(str(gap)+"\n")
        gaps_file.close()

        for weight in weight_samples[i]:
            weights_file.write(str(weight)+"\n")
        weights_file.close()

        for strength in strength_samples[i]:
            strengths_file.write(str(strength)+"\n")
        strengths_file.close()

        for size in total_comp_sizes[i]:
            compsizes_file.write(str(size)+"\n")
        compsizes_file.close()


print("--------------------------------------------------------------------------------")

runs_gdur = []
runs_size = []
runs_interacting = []
runs_duration = []
runs_kT = []

links_samples = dict()

fread = open(files_path+"/readme.txt", "w")

for i in interacting_dist:
    if type_list[i] == "simulation":
        runs_gdur.append(np.mean(g_durations[i]))
        runs_size.append(max(total_comp_sizes[i]))
        runs_interacting.append(np.mean(interacting_dist[i]))
        runs_duration.append(np.mean(durations_list[i]))
        runs_kT.append(np.mean(kT_samples[i]))
    elif type_list[i] == "dataset":
        print("Dataset - Number of nodes: "+str(num_nodes[i]))
        fread.write("Dataset - Number of nodes: "+str(num_nodes[i])+"\n")
        print("Dataset - Average Number of interacting nodes per slot: "+str(np.mean(interacting_dist[i])))
        fread.write("Dataset - Average Number of interacting nodes per slot: "+str(np.mean(interacting_dist[i]))+"\n")
        print("Dataset - Average aggregated degree: "+str(np.mean(kT_samples[i])))
        fread.write("Dataset - Average aggregated degree: "+str(np.mean(kT_samples[i]))+"\n")
        print("Dataset - Average contact duration: "+str(np.mean(durations_list[i])))
        fread.write("Dataset - Average contact duration: "+str(np.mean(durations_list[i]))+"\n")
        print("Dataset - Largest Component: "+str(max(total_comp_sizes[i])))
        fread.write("Dataset - Largest Component: "+str(max(total_comp_sizes[i]))+"\n")

    links_samples[i] = []
    for s in links_slots[i]:
        links_samples[i].append(links_slots[i][s])

runs_links = []
for i in links_samples:
    if type_list[i] == "simulation":
        runs_links.append(np.mean(links_samples[i]))
    elif type_list[i] == "dataset":
        print("Dataset - Average Number of links per slot: "+str(np.mean(links_samples[i])))
        fread.write("Dataset - Average Number of links per slot: "+str(np.mean(links_samples[i]))+")\n")

print("Simulations - Average Number of interacting nodes per slot: "+str(np.mean(runs_interacting)))
fread.write("Simulations - Average Number of interacting nodes per slot: "+str(np.mean(runs_interacting))+"\n")
print("Simulations - Average aggregated degree: "+str(np.mean(runs_kT)))
fread.write("Simulations - Average aggregated degree: "+str(np.mean(runs_kT))+"\n")
print("Simulations - Average contact duration: "+str(np.mean(runs_duration)))
fread.write("Simulations - Average contact duration: "+str(np.mean(runs_duration))+"\n")
print("Simulations - Largest Component: "+str(max(runs_size)))
fread.write("Simulations - Largest Component: "+str(max(runs_size))+"\n")
print("Simulations - Average Number of links per slot: "+str(np.mean(runs_links)))
fread.write("Simulations - Average Number of links per slot: "+str(np.mean(runs_links))+"\n")

print("--------------------------------------------------------------------------------")
fread.write("--------------------------------------------------------------------------------\n")

if True:
    avg_cyc_unis = []
    avg_cyc_tots = []
    avg_cyc_recs = []
    for i in cycle_unis:

        if type_list[i] == "dataset":
            print("Dataset  - Unique Components: "+str(cycle_unis[i]))
            fread.write("Dataset  - Unique Components: "+str(cycle_unis[i])+"\n")
            print("Dataset - Total Components: "+str(cycle_tots[i]))
            fread.write("Dataset - Total Components: "+str(cycle_tots[i])+"\n")
            print("Dataset - Recurrent Components per bin: "+str(cycle_avg[i]))
            fread.write("Dataset - Recurrent Components per bin: "+str(cycle_avg[i])+"\n")
        elif type_list[i] == "simulation":
            avg_cyc_unis.append(cycle_unis[i])
            avg_cyc_tots.append(cycle_tots[i])
            avg_cyc_recs.append(cycle_avg[i])

    avg_cyc_unis = np.mean(avg_cyc_unis)
    avg_cyc_tots = np.mean(avg_cyc_tots)
    std_cyc_recs = np.std(avg_cyc_recs)
    avg_cyc_recs = np.mean(avg_cyc_recs)

    print("Simulations - Average Unique Components: "+str(avg_cyc_unis))
    fread.write("Simulations - Average Unique Components: "+str(avg_cyc_unis)+"\n")
    print("Simulations - Average Total Components: "+str(avg_cyc_tots))
    fread.write("Simulations - Average Total Components: "+str(avg_cyc_tots)+"\n")
    print("Simulations - Average Recurrent Components per bin: "+str(avg_cyc_recs)+" std dev: "+str(std_cyc_recs))
    fread.write("Simulations - Average Recurrent Components per bin: "+str(avg_cyc_recs)+" std dev: "+str(std_cyc_recs)+"\n")

fread.close()
