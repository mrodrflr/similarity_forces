import matplotlib.pyplot as plt
import numpy as np
import os
import glob, sys
import random
from natsort import natsorted
import pathpy as pp
import argparse
import shutil

#finds all snapshots in a given directory
def get_file_list(dir_path):
    return natsorted(glob.glob(dir_path), key = lambda y: y.lower())

def read_raw(path):
    data_array = np.genfromtxt(path, dtype="int")
    return data_array

#stores all snapshots in a list
def store_edgelists(file_list, overlap_path, temp_dir):
    i = 0
    runs = 0
    legends = dict()
    paths_list = []
    type_list = dict()
    temp_path = os.path.join(overlap_path, temp_dir)
    if not os.path.exists(temp_path):
        os.makedirs(temp_path)

    headers = ["time", "source", "target"]
    for file1 in file_list:
        print("Processing "+str(file1)+" ...")
        file_name = file1.split('/')
        file_name = file_name[-1]
        tmp = open(temp_path+"/"+file_name, "w")
        ori = open(file1, "r")
        tmp.write(' '.join(headers) + "\n")
        for line in ori.readlines():
            tmp.write(line)
        tmp.close()
        ori.close()

    temp_list = get_file_list(temp_path+'/*.txt')
    for file1 in temp_list:
        work_t = pp.TemporalNetwork.readFile(file1, sep=' ')
        work_paths = pp.Paths.fromTemporalNetwork(work_t, delta=np.inf)
        paths_list.append(work_paths)
        file_name = file1.split('/')
        file_name = file_name[-1]

        if file1 == temp_path+"/remapped_edgelist_Hospital.txt":
            legends[i] = "Hospital"
            type_list[i] = "dataset"
        elif file1 == temp_path+"/remapped_edgelist_Primary.txt":
            legends[i] = "Primary School"
            type_list[i] = "dataset"
        elif file1 == temp_path+"/remapped_edgelist_HS2013.txt":
            legends[i] = "High School 2013"
            type_list[i] = "dataset"
        elif file1 == temp_path+"/remapped_edgelist_HyT2009.txt":
            legends[i] = "Conference"
            type_list[i] = "dataset"
        elif "MIT" in file_name:
            legends[i] = "MIT Social Evolution"
            type_list[i] = "dataset"
        elif "DATASET" in file_name or "dataset" in file_name:
            legends[i] = "Dataset"
            type_list[i] = "dataset"
        else:
            type_list[i] = "simulation"
            legends[i] = str(i+1)
            runs += 1

        i += 1

    return paths_list, legends, type_list, runs


#file naming
parser = argparse.ArgumentParser(description="Shortest time-respecting paths distribution plotter", epilog="Authors: Marco Antonio Rodriguez Flores and Fragkiskos Papadopoulos\nPublication: Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks (10.1103/PhysRevLett.121.258301)")
parser.add_argument('-o', '--output', help='Output Plot Name Suffix', required=True)
parser.add_argument('-l', '--legend', help='Legend for simulation averages in the plot', required=True)
parser.add_argument('-f', '--folder', help='Folder name/path that contains the input files', required=True)
args =parser.parse_args()

file_sufix = args.output
legend_avgs = args.legend
overlap_path = args.folder

files_path = os.path.join(overlap_path, "Raw Files")
graphs_path = os.path.join(overlap_path, "Plots")

if not os.path.exists(overlap_path):
    print(overlap_path+" folder not found! Create the folder and place the edgelists to process in it.")
    exit()

if not os.path.exists(files_path):
    os.makedirs(files_path)

if not os.path.exists(graphs_path):
    os.makedirs(graphs_path)

temp_dir = "temp"
paths_list, legends, type_list, runs = store_edgelists(get_file_list(overlap_path+'/*.txt'), overlap_path, temp_dir) #Reads all temporal edgelists in the overlap folder
print(type_list)

fig1 = plt.figure()
ax1 = fig1.add_subplot(1,1,1)
ax1.set_yscale('log')
plt.xlabel('shortest path length', fontsize=22)
plt.ylabel('distribution', fontsize=22)
labels = []

p_id = 0
pdf_avg = dict()
min_x = min_y = 999999
max_x = max_y = 0
for paths in paths_list:
    total = 0
    pdf = dict()
    if type_list[p_id] == "dataset":
        fpl = open(files_path+"/paths_"+legends[p_id]+".txt", "w")

    spaths = paths.getShortestPaths()

    for a in spaths:
        for b in spaths[a]:
            p_len = len(list(spaths[a][b])[0])
            if not p_len in pdf:
                pdf[p_len] = 0
            pdf[p_len] += 1
            total += 1

    for l in pdf:
        pdf[l] = pdf[l] / float(total)

        if type_list[p_id] == "dataset":
            fpl.write(str(l)+" "+str(pdf[l])+"\n")

    if min(pdf.keys()) < min_x:
        min_x = min(pdf.keys())
    if max(pdf.keys()) > max_x:
        max_x = max(pdf.keys())
    if min(pdf.values()) < min_y:
        min_y = min(pdf.values())
    if max(pdf.values()) > max_y:
        max_y = max(pdf.values())

    if type_list[p_id] == "dataset":
        fpl.close()
        label = ax1.scatter(list(pdf.keys()), list(pdf.values()), marker="s", label=legends[p_id], facecolors="none", edgecolors="red", s=60, linewidths=2, clip_on=False)
        labels.append(label)
    else:
        for l in pdf:
            if not l in pdf_avg:
                pdf_avg[l] = []
            pdf_avg[l].append(pdf[l])

    p_id += 1

sorted_lengths = list(pdf_avg.keys())
pdf_runs = []
fpl = open(files_path+"/paths_averages_"+file_sufix+".txt", "w")
for l in sorted_lengths:
    p_of_l = sum(pdf_avg[l]) / float(runs)
    pdf_runs.append(p_of_l)
    fpl.write(str(l)+" "+str(p_of_l)+"\n")

if min(sorted_lengths) < min_x:
    min_x = min(sorted_lengths)
if max(sorted_lengths) > max_x:
    max_x = max(sorted_lengths)
if min(pdf_runs) < min_y:
    min_y = min(pdf_runs)
if max(pdf_runs) > max_y:
    max_y = max(pdf_runs)

fpl.close()

ax1.set_xlim([min_x, max_x])
ax1.set_ylim([min_y, max_y])
label, = ax1.plot(sorted_lengths, pdf_runs, linestyle='--', label=legend_avgs, c="blue", lw=2, clip_on=False)
labels.append(label)

plt.legend(bbox_to_anchor=(1.0, 1.0), handles=labels, loc="upper right")
plt.savefig(graphs_path+'/Plot_shortest_pahts_'+file_sufix+'.pdf')
plt.close(fig1)

try:
    shutil.rmtree(overlap_path+"/"+temp_dir)
except OSError as e:
    print ("Error: %s - %s." % (e.filename, e.strerror))
