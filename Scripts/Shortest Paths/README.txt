Description:
	This script plots the distribution of the shortest time-respecting paths.
	The shortest paths are computed with the pathpy library while this script plots their distribution.

Requirements:
	Python3
	matplitlib
	numpy
	natsort library: https://pypi.org/project/natsort/ Please follow the instructions to set it up.
	pathpy library: https://ingoscholtes.github.io/pathpy/tutorial.html Please follow the instructions to set it up.


1) Usage:

	python3 time_paths.py -h to display the help menu

	python3 time_paths.py -o OUTPUT_NAME -l LEGEND -f INPUT_DIRECTORY 

	example: python3 -o FDM_Hosp -l "Force-dir. Motion" -f 'input path'

	See section 2 of this readme for more details about the argument values.


2) Required arguments:

-o OUTPUT_NAME / --output OUTPUT_NAME  : The output plot name sufix to add to all output files and plots : Type expected is 'string'
		The output plot produced will be named Plot_shortest_pahts_[OUTPUT_NAME].pdf

-l LEGEND / --legend LEGEND  : The legend to use for simulation results in the plots	  : Type expected is 'string'
		The text to use for simulation results in the plots' legend box.
	
-f INPUT_DIRECTORY / --folder INPUT_DIRECTORY  : The folder name or path where the input files are located  : Type expected is 'string'
		The name of the folder that contains the input files.
		The script will read all the files in the folder with .txt extension.
		The dataset edgelists currently recognized must be named: remapped_edgelist_Hospital.txt, remapped_edgelist_Primary.txt, remapped_edgelist_HS2013.txt and remapped_edgelist_HyT2009.txt
		For new datasets please include the keyword 'DATASET' or 'dataset' in the file name.
		The rest of the files in the folder not recognized as datasets will be treated as simulation files.

3) Output:
	The script creates 2 folders inside the input folder and contain the following output files:
		1) Plots: it contains the output plot of the shortest paths distribution.
		2) Raw_Files: it contains the raw files of the shortest paths distribution. The format of the file is: column 1 is the value on the x-axis and column 2 is the value on the y-axis.

--------------------------------------------------------------------------------------------------
Authors: Marco Antonio Rodriguez Flores & Fragkiskos Papadopoulos
Publication: Similarity Forces and Recurrent Components in Human Face-to-Face Interaction Networks
doi: 10.1103/PhysRevLett.121.258301
